# Schematisation Base de donnée

Table: User N-1 (Equipe + categorie + pays)
+ ID 
+ prenom
+ nom
+ mail
+ password
+ equipe
+ code_pays
+ id_categorie


Table: Team  1-N (joueurs) N-1 (pays + categorie)
+ ID
+ nom
+ id_categorie
+ code_pays



Table: Categorie 1-N (Team & joueurs)
+ ID 
+ sport

Table: Pays 1-N (Team & Joueurs)
+ code
+ alpha3
+ nom
+ flag (png)


# Creation de la BDD via phpmyadmin 

Lancement du serveur XAMP 
Création de la base de données via PhpMyAdmin 
Création des 4 tables + champs
id de chaque table en Auto Increment sauf pays en code numerique unique 
User en réalité joueur => Relation avec Equipe N-1  
Ajout des clés étrangères avec les 4 tables

points d'entrées à mettre en place : 

    www.monsite.fr/joueurs/
    www.monsite.fr/joueurs/:id
    www.monsite.fr/joueurs/:teams
    www.monsite.fr/joueurs/:pays
    www.monsite.fr/joueurs/:sport

    www.monsite.fr/teams/
    www.monsite.fr/teams/:id
    www.monsite.fr/teams/:pays
    www.monsite.fr/teams/:sport
    