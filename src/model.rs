extern crate serde;

use serde::Serialize;

#[derive(Serialize)]
pub struct Sports {
    pub nb_players: i64,
    pub sport: String,
}

#[derive(Serialize, Deserialize)]
pub struct Sport {
    pub id: i32,
    pub sport: String,
}
#[derive(Serialize, Deserialize)]
pub struct Sport2 {
    pub id: i32,
    pub sport: String,
    pub nb_players: i64,
}

#[derive(Serialize, Deserialize)]
pub struct Joueur {
    pub id_joueur: i32,
    pub nom: String,
    pub prenom: String,
    pub equipe: String,
    pub sport: String,
    pub pays: String,
}

#[derive(Serialize, Deserialize)]
pub struct Equipe {
    pub id_equipe: i32,
    pub team_name: String,
    pub sport: String,
    pub country_name: String,
}

